import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Task1();
        Task2();
    }

    public static void Task1() {
        System.out.println("Task1");
        System.out.println("Task1.1");
        int[] array = new int[] {12, 23, 35, 5, 456, 7, 123, 321, 10};
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j]) {
                    int temp;
                    temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }

        for (int a : array) {
            System.out.print(a + " ");
        }

        System.out.println();

        for (int i = array.length - 1 ; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }

        //Task2
        System.out.println("\n\nTask1.2");
        for (int i = 0; i < array.length; i++) {
            String temp = "" + array[i];
            boolean out = false;
            for (int j = 0; j < temp.length() - 1; j++) {
                int numj = (int)temp.charAt(j);
                for (int k = j + 1; k < temp.length(); k++) {
                    int numk = (int)temp.charAt(k);
                    if (numj + 1 == numk) {
                        out = true;
                    }
                }
            }
            if (out) {
                System.out.println(array[i]);
            }
        }

        //Task3
        System.out.println("\nTask1.3");
        String[] months = new String[] { "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" };
        Scanner in = new Scanner(System.in);
        System.out.print("Введите число: ");
        try {
            int number = in.nextInt();
            if (number > 0 && number < 13)
                System.out.println(months[number-1]);
            else
                System.out.println("Неверный ввод.");
        }
        catch (Exception ex) {
            System.out.println("Неверный ввод.");
        }
    }

    public static void Task2() {
        System.out.println("\n\nTask2;");
        System.out.println("Task2.1;");

        int[][] matrix = new int[][] { { 2, 1, 5, 7 }, { 4, 6, 3, 8}, { 1, 9, 4, 9}, { 1, 9, 3, 2}};
        List<Integer> tempMatrix = new ArrayList<Integer>();

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                tempMatrix.add(matrix[i][j]);
            }
        }
        Collections.sort(tempMatrix);

        System.out.println("Исходная матрица:");
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("\nПо возрастанию:");
        int k = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                matrix[i][j] = tempMatrix.get(k++);
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("\nПо убыванию:");
        for (int i = 3; i >= 0; i--) {
            for (int j = 3; j >= 0; j--) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("\nTask2.2");
        System.out.println("Исходная матрица:");
        matrix = new int[][] { { 2, 1, 5, 7 }, { 4, 6, 3, 8}, { 1, 9, 4, 9}, { 1, 9, 3, 2}};
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }

        int max = matrix[0][0];
        for (int i = 1; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (matrix[i][j] > max)
                    max = matrix[i][j];
            }
        }

        List<Integer> columns = new ArrayList<Integer>();
        List<Integer> rows = new ArrayList<Integer>();

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (matrix[i][j] == max) {
                    if (!rows.contains(i)) {
                        rows.add(i);
                    }
                    if (!columns.contains(j)) {
                        columns.add(j);
                    }
                }
            }
        }

        Collections.sort(columns);
        Collections.sort(rows);

        List<Integer> tempList = new ArrayList<Integer>();

        for (int i = 0; i < 4; i++) {
            if (!rows.contains(i)) {
                for (int j = 0; j < 4; j++) {
                    if (!columns.contains(j)) {
                        tempList.add(matrix[i][j]);
                    }
                }
            }
        }

        System.out.println();

        System.out.println("Матрица без строк и столбцов, содержащих максимальный элемент:");
        k = 0;
        int[][] matrixWithoutMax = new int[4-rows.size()][4-columns.size()];
        for (int i = 0; i < 4 - rows.size(); i++) {
            for (int j = 0; j < 4 - columns.size(); j++) {
                matrixWithoutMax[i][j] =  tempList.get(k++);
                System.out.print(matrixWithoutMax[i][j] + " ");
            }
            System.out.println();
        }
    }
}